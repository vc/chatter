function links(parent, args, ctx, info) {
  const { linkIds } = parent;
  return ctx.db.query.links({ where: { id_in: linkIds } }, info);
}

module.exports = {
  links,
};
