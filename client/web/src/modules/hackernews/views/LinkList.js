import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Link from './Link';

class LinkList extends Component {
  render() {
    const linksToRender = [
      {
        id: '1',
        description: 'Test',
        url: 'http://www.test.com',
      },
      {
        id: '2',
        description: 'GraphQL',
        url: 'http://www.graphql.com',
      }
    ];

    return (
      <div>
        {linksToRender.map(link => <Link key={link.id} link={link} />)}
      </div>
    );
  }
}

export default LinkList;
