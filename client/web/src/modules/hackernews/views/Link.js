import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Link extends Component {
  static propTypes = {
    link: PropTypes.shape({
      description: PropTypes.string,
      url: PropTypes.string,
    }),
  };
  
  _voteForLink = async () => {
    //// TODO
  };

  render() {
    return (
      <div>
        <div>{this.props.link.description} ({this.props.link.url})</div>
      </div>
    );
  }
}

export default Link;
