import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { views as AppViews } from './modules/app';
import registerServiceWorker from './registerServiceWorker';

const App = AppViews.App;

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
